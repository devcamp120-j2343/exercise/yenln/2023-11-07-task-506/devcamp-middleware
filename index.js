//khai báo thư viện express

const express = require("express");

//khai báo app 
const app = express();

//khai báo port
const port = 8000;

const methodMiddleware = (req,res,next) =>{
    console.log(req.method);
    
    next();
}

app.use((req, res,next)=>{
    console.log(new Date());
    next();
}, methodMiddleware);

app.post("/", (req,res) =>{
    console.log("Post method");
    res.status(200).json({
        message: 'Post method'
    })
})

app.get("/", (req,res) =>{
    let  today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`);
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`
    })
})

//khởi động app
app.listen(port, () => { 
    console.log(`App listen on port ${port}`);
})